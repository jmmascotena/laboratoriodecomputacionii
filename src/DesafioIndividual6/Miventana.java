package Desafio6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

class Miventana {
    public static void main(String[] arg) {
        Marco marco = new Marco();

    }
}

class Marco extends JFrame{
    public Marco (){
        Toolkit pantalla = Toolkit.getDefaultToolkit();
        Dimension tamanoPantalla = pantalla.getScreenSize();
        int alturaPantalla = tamanoPantalla.height;
        int anchoPantala = tamanoPantalla.width;
        setBounds(anchoPantala/4,alturaPantalla/4,anchoPantala/2,alturaPantalla/2);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Mi primer ventana");
        Image miIcono= pantalla.getImage("D:\\RepoGitLab\\laboratoriodecomputacionii\\src\\Desafio6\\UTN_logo.jpg");
        setIconImage(miIcono);
        Panel miPanel = new Panel();
        add(miPanel);
        miPanel.setBackground(new Color(105,196,154));


    }


}

class Panel extends JPanel {
    JButton boton = new JButton("BOTON1");
    JButton boton2 = new JButton("BOTON2");
    public Panel(){
        add(boton);
        add(boton2);
        EventoDeFoco foco = new EventoDeFoco();
        boton.addFocusListener(foco);
        boton2.addFocusListener(foco);
        EventoDeMouse mouse = new EventoDeMouse();
        addMouseListener(mouse);
    }


   public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        Font miFuente = new Font("Verdana", Font.ITALIC,12);
        g2.setFont(miFuente);
        g2.drawString("Bienvenido/a, en esta venta se presentara enventos a los botones y al panel",100,100);
       g2.drawString("en el cual cuando hago click sobre el mismo,este emitira un mensaje en la consola,",100,110);
       g2.drawString("lo mismo cuando alguno de los botones ganen foco emitira un mensaje en la consola",100,120);

        boton.setBounds(120,10, 150,30);
        boton2.setBounds(120,50,150,30);



   }

   private class EventoDeFoco implements FocusListener{

       @Override
       public void focusGained(FocusEvent e) {
           if(e.getSource() == boton){
               System.out.println("BOTON1 ha ganado foco");
           }
           else{
               System.out.println("BOTON2 ha ganado foco");
           }
       }

       @Override
       public void focusLost(FocusEvent e) {
           if(e.getSource() == boton){
               System.out.println("BOTON1 ha perdido foco");
           }
           else{
               System.out.println("BOTON2 ha perdido foco");
           }
       }
   }
   class EventoDeMouse implements MouseListener{

       @Override
       public void mouseClicked(MouseEvent e) {
           System.out.println("Has hecho click");
       }

       @Override
       public void mousePressed(MouseEvent e) {

       }

       @Override
       public void mouseReleased(MouseEvent e) {

       }

       @Override
       public void mouseEntered(MouseEvent e) {

       }

       @Override
       public void mouseExited(MouseEvent e) {

       }
   }


}




