package Banco;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

    public class Banco {

        public static void main(String[]args){

            CuentaCorriente cta1 = new CuentaCorriente("Juan Manuel", 1000, 258741);
            CuentaCorriente cta2 = new CuentaCorriente("Roxana", 2000, 951159);

            Set<CuentaCorriente> listadoDeCeuntas = new HashSet<>();
            listadoDeCeuntas.add(cta1);
            listadoDeCeuntas.add(cta2);

            Operacion.Transferir(cta1, cta2, 500);

            System.out.println("Salida: "+listadoDeCeuntas.toString());

            try {
                ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                        "D:\\RepoGitLab\\laboratoriodecomputacionii\\src\\Banco\\listadoDeCeuntas.dat"));

                flujoDeSalida.writeObject(listadoDeCeuntas);
                flujoDeSalida.close();

            } catch (Exception e) {

                e.printStackTrace();
            }

            try {

                ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                        "D:\\RepoGitLab\\laboratoriodecomputacionii\\src\\Banco\\listadoDeCeuntas.dat"));

                Set<CuentaCorriente> listadoDeCeuntasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();

                System.out.println("Entrada: "+ listadoDeCeuntasEntrada.toString());

                flujoDeEntrada.close();

            } catch (Exception e) {

                e.printStackTrace();
            }

        }

    }
