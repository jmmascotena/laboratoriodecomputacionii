package Banco;

class Operacion {
    public static void Transferir(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto){
        cuentaEgreso.sacarDinero(monto);
        cuentaIngreso.igresarDinero(monto);
    }
}
