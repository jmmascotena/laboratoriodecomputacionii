package Introduccion;

public class Api_Math {
    public static void main(String[] args) {
        double cos = Math.cos(90);
        double sen = Math.sin(45);
        double tan = Math.tan(45);
        double atan = Math.atan(25);
        double atan2 = Math.atan2(4,5);

        double exp = Math.exp(3);
        double log = Math.log(exp);

        final double pi = Math.PI;
        final double e = Math.E;

        System.out.println("TRIGONOMETRICAS:");
        System.out.println("cos = "+cos);
        System.out.println("sen = "+sen);
        System.out.println("tan = "+tan);
        System.out.println("atan = "+atan);
        System.out.println("atan2 = "+atan2);

        System.out.println("EXPONENCIAL Y SU INVERSA:");
        System.out.println("exp = "+exp);
        System.out.println("log = "+log);

        System.out.println("PI Y E:");
        System.out.println("pi = "+pi);
        System.out.println("e = "+e);

    }
}
