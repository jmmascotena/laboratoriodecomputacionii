package Desafio11;

import javax.swing.*;
import java.util.Random;

public class UsoCuenta {

    public static void main(String[] arg) {

        CuentaCorriente cuenta1 = new CuentaCorriente(5000,"Juan ");
        CuentaCorriente cuenta2 = new CuentaCorriente(4000,"Maria");
        CuentaCorriente.Transferencia(cuenta1,cuenta2,2500);
        cuenta1.mostrarTodosLosDatos();
        cuenta2.mostrarTodosLosDatos();

    }
}
    class CuentaCorriente{
        private double saldo;
        private String titular;
        private long nrocuenta;

        public CuentaCorriente(double saldo, String titular){
            this.saldo = saldo;
            this.titular = titular;

            Random aleatorio = new Random();
            this.nrocuenta = Math.abs(aleatorio.nextLong());
        }

        public void geterSaldo(){
            JOptionPane.showMessageDialog(null,"El saldo de la cuenta es: "+ saldo);
        }


        public void mostrarTodosLosDatos() {
             JOptionPane.showMessageDialog(null, "Titular: "+ titular +"" + "\n Nro de cuenta: "+ nrocuenta +"\n Saldo: "+ saldo);
        }

        public void ingresarDinero(double dinero){
            saldo+=dinero;
        }
        public void retirarDinero(double dinero){
            saldo -= dinero;
        }

        public static void Transferencia(CuentaCorriente cuenta1, CuentaCorriente cuenta2, double dinero){
            cuenta1.saldo -= dinero;
            cuenta2.saldo += dinero;

        }
        
    }



